import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { HeroesListsComponent } from './heroes-lists.component';

describe('HeroesListsComponent', () => {
  let component: HeroesListsComponent;
  let fixture: ComponentFixture<HeroesListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports : [FormsModule],
      declarations: [ HeroesListsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeroesListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  })
});
